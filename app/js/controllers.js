'use strict';

/* Controllers */
var app = angular.module("app", ["xeditable"]);
angular.module('testApp', ["xeditable"]).controller('testAppCtlr', ['$scope', function($scope) {
  var todoList = this;
  todoList.todos = [
    {'title': 'title1',
     'info': 'textextexte textextextetextextexte textextexte'},
    {'title': 'title2',
     'info': 'textextexte textextextetextextexte textextexte textextexte textextextetextextexte textextexte textextexte textextextetextextexte textextexte textextexte textextextetextextexte textextexte'}
  ];
  todoList.process = [
    {'title': 'title3',
     'info': 'textextexte textextextetextextexte textextexte textextexte textextextetextextexte textextexte'},
    {'title': 'title4',
     'info': 'textextexte textextextetextextexte textextexte'}
  ];
  todoList.done = [
    {'title': 'title5',
     'info': 'textextexte textextextetextextexte textextexte'},
    {'title': 'title6',
     'info': 'textextexte textextextetextextexte textextexte'}
  ];

  todoList.addTodo = function(){
  	todoList.todos.push({title:todoList.todoTitle, info:todoList.todoInfo});
  	todoList.todoTitle = '';
  	todoList.todoInfo = '';
  };
  todoList.deleteItem = function(ind, arr){
  	arr.splice(ind, 1);
  };

}]);

